import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cupertino Widgets',
      theme: ThemeData.dark(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DateTime? dateTime;
  Duration? time;
  bool cupertinoSwitch = true;

  double rating = 0;

  void datePickerCupertino(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) {
          return Container(
            height: 400.0,
            color: Colors.white,
            child: Column(
              children: [
                Container(
                  height: 300.0,
                  child: CupertinoDatePicker(
                    initialDateTime: DateTime.now(),
                    onDateTimeChanged: (val) {
                      setState(() {
                        dateTime = val;
                      });
                    },
                  ),
                ),
                CupertinoButton(
                  child: Text('ok'),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          );
        });
  }

  void timePickerCupertino(ctx) {
    showCupertinoModalPopup(
        context: ctx,
        builder: (_) {
          return Container(
            height: 400.0,
            color: Colors.black12,
            child: Column(
              children: [
                Container(
                  height: 300.0,
                  child: CupertinoTimerPicker(
                      initialTimerDuration: Duration.zero,
                      onTimerDurationChanged: (val) {
                        setState(() {
                          time = val;
                        });
                      }),
                ),
                CupertinoButton(
                  child: Text('ok'),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          );
        },);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(height: 50.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
             Text(dateTime==null?'Date':'$dateTime'),
              Text(time==null?'Time':'$time'),

            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () => datePickerCupertino(context),
                child: Text('Date picker'),
              ),
              ElevatedButton(
                onPressed: () => timePickerCupertino(context),
                child: Text('Time picker'),
              ),
            ],
          ),
          const SizedBox(height: 50.0),
          CupertinoSwitch(
            value: cupertinoSwitch,
            onChanged: (value) {
              setState(() {
                cupertinoSwitch = !cupertinoSwitch;
              });
            },
          ),
          CupertinoSlider(
            min: 0,
            max: 10,
            value: rating,
            onChanged: (newRatting) {
              setState(
                () {
                  rating = newRatting;
                  if (newRatting >= 10) {

                  } else if (newRatting <= 1) {

                  }
                },
              );
            },
          )
        ],
      ),
    );
  }
}
